package com.alfredo.cursos.exceptions;

public class CursoNaoEncontradoException extends RuntimeException {

    public CursoNaoEncontradoException(int id){
        super("Não foi possível encontrar o curso com id:  " + id);
    }
}
