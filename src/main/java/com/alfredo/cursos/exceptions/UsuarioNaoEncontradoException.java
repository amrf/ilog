package com.alfredo.cursos.exceptions;

public class UsuarioNaoEncontradoException extends RuntimeException {

    public UsuarioNaoEncontradoException(int id){
        super("Não foi possível encontrar o usuário com id:  " + id);
    }
}
