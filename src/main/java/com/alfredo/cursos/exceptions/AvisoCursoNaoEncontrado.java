package com.alfredo.cursos.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class AvisoCursoNaoEncontrado {

    @ResponseBody
    @ExceptionHandler(CursoNaoEncontradoException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String usuarioNaoEncontrado(CursoNaoEncontradoException ex) {
        return ex.getMessage();
    }
}
