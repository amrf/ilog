package com.alfredo.cursos.services;

import net.sf.jasperreports.engine.*;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class JasperReportsImpl implements JasperReports {

    @Override
    public byte[]  gerarReportPdf(Map<String, Object> params, JRDataSource dataSource) {
        byte[] bytes = null;

        JasperReport jasperReport = null;

        try {//
            jasperReport = JasperCompileManager.compileReport("src/main/resources/jasper/Users.jrxml");
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params,
                    dataSource);
            bytes = JasperExportManager.exportReportToPdf(jasperPrint);
        } catch (JRException e) {
            e.printStackTrace();
        }

        return bytes;
    }
}
