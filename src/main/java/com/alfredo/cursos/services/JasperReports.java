package com.alfredo.cursos.services;

import net.sf.jasperreports.engine.JRDataSource;

import java.util.Map;

public interface JasperReports {

    public byte[]  gerarReportPdf(Map<String, Object> params, JRDataSource dataSource);
}
