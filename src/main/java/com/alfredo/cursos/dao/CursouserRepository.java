package com.alfredo.cursos.dao;

import com.alfredo.cursos.model.CursoUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CursouserRepository extends JpaRepository<CursoUser, Integer>, JpaSpecificationExecutor<CursoUser> {

}