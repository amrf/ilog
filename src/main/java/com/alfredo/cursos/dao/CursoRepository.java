package com.alfredo.cursos.dao;

import com.alfredo.cursos.model.Curso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CursoRepository extends JpaRepository<Curso, Integer>, JpaSpecificationExecutor<Curso> {

}