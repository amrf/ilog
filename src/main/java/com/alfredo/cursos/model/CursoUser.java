package com.alfredo.cursos.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "cursouser")
public class CursoUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idcursouser", nullable = false)
    private Integer idcursouser;

    @Column(name = "userid")
    private Integer userid;

    @Column(name = "cursoid")
    private Curso cursoid;

    public void setIdcursouser(Integer idcursouser) {
        this.idcursouser = idcursouser;
    }

    public Integer getIdcursouser() {
        return idcursouser;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getUserid() {
        return userid;
    }

    public Curso getCursoid() {
        return cursoid;
    }

    public void setCursoid(Curso cursoid) {
        this.cursoid = cursoid;
    }

    @Override
    public String toString() {
        return "Cursouser{" +
                "idcursouser=" + idcursouser + '\'' +
                "userid=" + userid + '\'' +
                "cursoid=" + cursoid + '\'' +
                '}';
    }
}
