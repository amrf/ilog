package com.alfredo.cursos.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "curso")
public class Curso implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idcurso", nullable = false)
    private Integer idcurso;

    @Column(name = "titulo")
    private String titulo;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "cargahoraria")
    private Integer cargahoraria;

    @Column(name = "valor")
    private Integer valor;

    @ManyToMany
    @JoinColumn(name = "id")
    @JsonProperty("Usuarios")
    private Set<User> user;

    public void setIdcurso(Integer idcurso) {
        this.idcurso = idcurso;
    }

    public Integer getIdcurso() {
        return idcurso;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setCargahoraria(Integer cargahoraria) {
        this.cargahoraria = cargahoraria;
    }

    public Integer getCargahoraria() {
        return cargahoraria;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

    public Integer getValor() {
        return valor;
    }

    public Set<User> getUser() {
        return user;
    }

    public void setUser(Set<User> user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Curso{" +
                "idcurso=" + idcurso + '\'' +
                "titulo=" + titulo + '\'' +
                "descricao=" + descricao + '\'' +
                "cargahoraria=" + cargahoraria + '\'' +
                "valor=" + valor + '\'' +
                '}';
    }
}
