package com.alfredo.cursos.resources;

import com.alfredo.cursos.dao.UserRepository;
import com.alfredo.cursos.exceptions.UsuarioNaoEncontradoException;
import com.alfredo.cursos.model.User;
import com.alfredo.cursos.services.JasperReportsImpl;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    JasperReportsImpl jasperReportsImpl;

    @GetMapping("/usuario")
    @ResponseBody
    public ResponseEntity<List<User>> listarUsuarios() {
        return new ResponseEntity<List<User>>(userRepository.findAll(), HttpStatus.OK);
    }

    @PostMapping("/usuario")
    @ResponseBody
    public ResponseEntity<User> novousuario(@RequestBody User user) {
        return new ResponseEntity<User>(userRepository.save(user), HttpStatus.OK);
    }

    @GetMapping("/usuario/{id}")
    public ResponseEntity<Optional<User>> listarusuario(@PathVariable int id) {
        return new ResponseEntity<Optional<User>>(Optional.ofNullable(userRepository.findById(id).orElseThrow(() -> new UsuarioNaoEncontradoException(id))), HttpStatus.OK);
    }


    @DeleteMapping("/usuario/{id}")
    public String deleteEmployee(@PathVariable int id) {
        try {
            userRepository.deleteById(id);
        } catch (EmptyResultDataAccessException exception) {
            return exception.getLocalizedMessage();
        }
        return "Usuário " + id + " deletado com sucesso";
    }

    @GetMapping("/relatorio")
    @ResponseBody
    public ResponseEntity<byte[]> relatorioUsers() {

        Map<String, Object> parameters = new HashMap<String, Object>();

        byte[] bytes = jasperReportsImpl.gerarReportPdf(parameters, new JRBeanCollectionDataSource(userRepository.findAll()));

        ContentDisposition contentDisposition = ContentDisposition.builder("inline")
                .filename("Users.pdf").build();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentDisposition(contentDisposition);
        return ResponseEntity
                .ok()
                .header("Content-Type", "application/pdf; charset=UTF-8")
                .headers(headers)
                .body(bytes);


    }
}
