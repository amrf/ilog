package com.alfredo.cursos.resources;

import com.alfredo.cursos.dao.CursoRepository;
import com.alfredo.cursos.dao.CursouserRepository;
import com.alfredo.cursos.exceptions.CursoNaoEncontradoException;
import com.alfredo.cursos.model.Curso;
import com.alfredo.cursos.model.CursoUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class CourseController {

    @Autowired
    CursoRepository cursoRepository;

    @Autowired
    CursouserRepository cursouserRepository;

    @GetMapping("/curso")
    @ResponseBody
    public ResponseEntity<List<Curso>> listarCursos() {
        return new ResponseEntity<List<Curso>>(cursoRepository.findAll(), HttpStatus.OK);
    }

    @PostMapping("/curso")
    @ResponseBody
    public ResponseEntity<Curso> novousuario(@RequestBody Curso curso) {
        return new ResponseEntity<Curso>(cursoRepository.save(curso), HttpStatus.OK);
    }

    @GetMapping("/curso/{id}")
    public ResponseEntity<Optional<Curso>> listarusuario(@PathVariable int id) {
        return new ResponseEntity<Optional<Curso>>(Optional.ofNullable(cursoRepository.findById(id).orElseThrow(() -> new CursoNaoEncontradoException(id))), HttpStatus.OK);
    }


    @DeleteMapping("/curso/{id}")
    public String deleteEmployee(@PathVariable int id) {
        try {
            cursoRepository.deleteById(id);
        } catch (EmptyResultDataAccessException exception) {
            return exception.getLocalizedMessage();
        }
        return "Curso " + id + " deletado com sucesso";
    }


    //Associações de curso e usuário;

    @GetMapping("/associacoes")
    @ResponseBody
    public ResponseEntity<List<CursoUser>> listarAssociacoes() {
        return new ResponseEntity<List<CursoUser>>(cursouserRepository.findAll(), HttpStatus.OK);
    }

    @PostMapping("/associacoes")
    @ResponseBody
    public ResponseEntity<CursoUser> novousuario(@RequestBody CursoUser cursouser) {
        return new ResponseEntity<CursoUser>(cursouserRepository.save(cursouser), HttpStatus.OK);
    }
//

    @DeleteMapping("/associacoes/{id}")
    public String deleteCursoUsuario(@PathVariable int id) {
        try {
            cursoRepository.deleteById(id);
        } catch (EmptyResultDataAccessException exception) {
            return exception.getLocalizedMessage();
        }
        return "Associação " + id + " deletada com sucesso";
    }


}
