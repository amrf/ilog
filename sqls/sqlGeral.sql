grant ALL PRIVILEGES ON *.* TO 'amrfteste';

CREATE TABLE `curso` (
                         `idcurso` int NOT NULL AUTO_INCREMENT,
                         `titulo` varchar(45) DEFAULT NULL,
                         `descricao` varchar(255) DEFAULT NULL,
                         `cargahoraria` int DEFAULT NULL,
                         `valor` int DEFAULT NULL,
                         PRIMARY KEY (`idcurso`),
                         UNIQUE KEY `idcurso_UNIQUE` (`idcurso`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `user` (
                        `id` int NOT NULL AUTO_INCREMENT,
                        `nome` varchar(255) DEFAULT NULL,
                        `telefone` varchar(45) DEFAULT NULL,
                        `endereco` varchar(500) DEFAULT NULL,
                        `dataadesao` datetime DEFAULT NULL,
                        `login` varchar(45) DEFAULT NULL,
                        `senha` varchar(45) DEFAULT NULL,
                        PRIMARY KEY (`id`),
                        UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `curso_user` (
                              `idcursouser` int NOT NULL AUTO_INCREMENT,
                              `user_id` int DEFAULT NULL,
                              `curso_idcurso` int DEFAULT NULL,
                              PRIMARY KEY (`idcursouser`),
                              UNIQUE KEY `idcursouser_UNIQUE` (`idcursouser`),
                              KEY `userid_idx` (`user_id`),
                              CONSTRAINT `cursoid` FOREIGN KEY (`idcursouser`) REFERENCES `curso` (`idcurso`),
                              CONSTRAINT `userid` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


INSERT INTO `cursoamrf`.`curso` (`titulo`, `descricao`, `cargahoraria`, `valor`) VALUES ('Java', 'Curso de Java', '20', '33');
INSERT INTO `cursoamrf`.`user` (`nome`, `telefone`, `endereco`, `dataadesao`, `login`, `senha`) VALUES ('Testelog', '11 22111211', 'Av. Paulista 1000', '2020-10-10', '121', '212');

